
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , db   = require('./model/crowdDb').db
  , path = require('path')
  , cors =  require('cors');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);;
app.use(express.static(path.join(__dirname, 'public')));

//Cors Enabling
app.use(cors());

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/*app.use(function(req,res,next){
    req.db = db;
    next();
});*/

app.get('/', routes.index);
app.use('/loginModule',  require('./controllers/login.controller'));
app.use('/register',require('./controllers/register.controller'));
//app.get('/users',require('./controllers/users.controller'));
app.use('/app',require('./controllers/compDetails.controller'));

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Connected to mongo database successfully.");
  http.createServer(app).listen(app.get('port'), function(){
	  console.log('Express server listening on port ' + app.get('port'));
	});
});

/*http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});*/
