var express = require('express');
var router = express();
var schema = require('../model/schema');

router.get('/', function (req, res) {
	res.render('companyLikes');
});

router.get('/getCompanyCards', function (req, res) {
	schema.CompDetails.find({}, function(err, users) {
		  if (err){ 
			 throw err;
		  }
		  res.status(200).json(users);
		  console.log(users);
	});
});

router.post('/setCompanyLikes', function(req,res){
	var query = {companyId: req.body.companyId};
	var newLikes = {likes: req.body.likes};
	schema.CompDetails.findOneAndUpdate(query, newLikes, {upsert:true}, function(err, doc){
	    if (err) return res.send(500, { error: err });
	    return res.send("succesfully saved");
	});
});
 
module.exports = router;