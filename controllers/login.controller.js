/*var express = require('express');
var router = express();
var schema = require('../model/schema');
var session = require('express-session');

 
router.post('/login', function (req, res, next) {
	   var email = req.body.email;
	   var pass = req.body.pass;
	   
	   schema.UserReg.findOne({email: email, pass: pass}, function(err, user) {
	      if(err) return next(err);
	      if(!user) return res.send('Not logged in!');
	      
	      console.log("User is:-"+user);
	     // req.session.user = email;
	      return res.send('Logged In!');
	   });
	});

     router.get('/logout', function (req, res) {
	   req.session.user = null;
	   console.log('Logged out successfully!');
	}); 
	
module.exports = router; */

'use strict';

var passport      = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var express = require('express');
var router = express();
var schema = require('../model/schema');
var session = require('express-session');

router.post('/login', function (req, res, next) {
	function localStrategy(User, config) {
    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'pass'
        },
        function(email, password, callback) {
            User.findOne({
                email: email.toLowerCase()
            }, function(err, user) {
                if (err) return callback(err);

                // no user found with that email
                if (!user) {
                    return res.send("The email is not registered"); 
                    //callback(null, false, { message: 'The email is not registered.' });
                }
                // make sure the password is correct
                user.comparePass(pass, function(err, isMatch) {
                    if (err) { return callback(err); }

                    // password did not match
                    if (!isMatch) {
                        return res.send("The password is not correct.");
                        //callback(null, false, { message: 'The password is not correct.' });
                    }

                    // success
                    return res.send("User Logged In!");
                    //callback(null, user);
                });
            });
        }
    ));
}
}); 

module.exports = router;