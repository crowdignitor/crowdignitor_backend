var mongoose = require('mongoose');
var autoIncrement= require('./crowdDb').autoInc;

//Schema of Collections in mongoDB 
var RegSchema = mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    mobNo:Number,
    pass: String,
}, { collection: 'UserRegistration' });

var CompanySchema = mongoose.Schema({
	companyId : Number,
	companyHeading : String,
	companyName : String,
	endorsement : Number,
	likes : Number,
	videoUrl: String,
	funding : String,
	foundingDate : Date,
	details : String
	
}, { collection: 'CompanyDetails' });

// Models
var UserReg = mongoose.model('UserReg', RegSchema);
RegSchema.plugin(autoIncrement.plugin, { model: 'UserReg', field: 'companyId' });

var CompDetails = mongoose.model('CompDetails',CompanySchema);
	
module.exports = {
	UserReg : UserReg,
	CompDetails : CompDetails	
};